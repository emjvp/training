#!/usr/bin/python3
""" Codeabbey 93: Starving priority queue
*** Sample run with DATA.lst
$ python raballestasr.py
11153064781
*** Linter output
$ pylint raballestasr.py 
No config file found, using default configuration
************* Module raballestasr
C: 64, 0: Final newline missing (missing-final-newline)
------------------------------------------------------------------
Your code has been rated at 9.62/10 (previous run: 8.85/10, +0.77)
"""
import heapq

# Parameters for random queue degree generation
A = 445
C = 700001
M = 2097152
N = 7
MAX_DEGREE = 999

def sim_queue(visitors, seed):
    """ Simulates the starving priority queue
    """
    queue = []
    total_discomfort = 0
    rand = seed
    #random.seed(seed)
    for time in range(2*visitors + 1):
        # Generate a random number using linear congruential generator and
        # reduce it modulo MAX_DEGREE to give the degree for entering the queue
        # and multiplied by -1 since Python's heaps are "min heaps"
        rand = (A*rand + C) % M
        starvation = -(1 + rand % MAX_DEGREE)
        # At positive even time a person is fed and leaves the queue
        if not (time % 2 or time == 0):
            fed_person = heapq.heappop(queue)
            # Discomfort is defined as starvation times the time spent in queue
            person_discomfort = fed_person[0]*(time - fed_person[1])
            total_discomfort += person_discomfort
        # Only add to queue as long as there are still visitors.
        # After that they just wait to be fed.
        if time < visitors:
            heapq.heappush(queue, (starvation, time))
    return -total_discomfort

def main():
    """ Read codeabbey usual test cases
    """
    data = open("DATA.lst", "r")
    visitors, seed = list(map(int, data.readline().split()))
    print(sim_queue(visitors, seed))
    data.close()

main()
