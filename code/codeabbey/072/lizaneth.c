/*
$ cppcheck --enable=all --inconclusive --language=c lizaneth.c #linting
$ gcc lizaneth.c -o exe #compilation
$ c lizaneth.c
$
*/

static int FunnyWordsG (int Xcur, int N){
    int Xnext = 0;
    int index = 0;
    int i = 0;
    char cons[] ={'b','c','d','f','g','h','j','k','l','m','n','p','r','s','t',
    'v','w','x','z','\0'};
    char vow[]={'a','e','i','o','u','\0'};
    char word[N];
    for (i=1; i<=N; i++){
        Xnext = (445 * Xcur + 700001) % 2097152;
        if (i % 2 != 0){
            index = Xnext % 19;
            word[i-1] = cons[index];
        }
        else{
            index = Xnext % 5;
            word[i-1] = vow[index];
        }
        Xcur = Xnext;
    }
    word[N] = '\0';
    printf("%s ",word);
    return Xnext;
}

int main(){
    int w = 0;
    int X0 = 0;
    int N =0;
    int i = 0;
    FILE *file = 0;
    if ((file = fopen("DATA.lst","r")) == NULL){
        printf("Error");
        exit(0);
    }
    (void)fscanf(file, "%d", &w);
    (void)fscanf(file, "%d", &X0);
    for (i=0; i<w; i++){
        (void)fscanf(file, "%d", &N);
        X0 = FunnyWordsG(X0,N);
    }
    (void)fclose(file);
    return 0;
}

/*
$ ./exe
hoj ladog xopim mipu direco sizucowo cebagebe kezuv jep cocov misu lovasaf
pebolo fici teb vimok bejis dipanat hasekad cunig kukipada hehaho
*/
