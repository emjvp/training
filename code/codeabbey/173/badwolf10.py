#!/usr/bin/env python
'''
$ pylint badwolf10.py #linting
No config file found, using default configuration
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ python badwolf10.py #compilation
'''

CHORDICT = [[0, 7, 3, 4], [1, 8, 4, 5],
            [2, 9, 5, 6],
            [3, 10, 6, 7],
            [4, 11, 7, 8],
            [5, 0, 8, 9],
            [6, 1, 9, 10],
            [7, 2, 10, 11],
            [8, 3, 11, 0],
            [9, 4, 0, 1],
            [10, 5, 1, 2],
            [11, 6, 2, 3]]

NOTES = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']

N = int(input())
OUTPUT = ""
for i in range(N):
    CHORDNOTES = [int(x) for x in raw_input().split()]
    NOTEPRESENCE = [[0 for col in range(4)] for row in range(12)]
    for j, _n in enumerate(CHORDNOTES, 0):
        NOTE = CHORDNOTES[j] % 12
        for k in range(12):
            for l in range(4):
                if CHORDICT[k][l] == NOTE:
                    NOTEPRESENCE[k][l] += 1
    CHORD = ""
    NOTESINCHORD = 0
    for m in range(12):
        NOTESINCHORD = 0
        for n in range(4):
            if NOTEPRESENCE[m][n] > 0:
                NOTESINCHORD += 1
        if NOTESINCHORD >= 3:
            if NOTEPRESENCE[m][2] > 0 and NOTEPRESENCE[m][3] == 0:
                CHORD = NOTES[m] + "-minor"
            elif NOTEPRESENCE[m][2] == 0 and NOTEPRESENCE[m][3] > 0:
                CHORD = NOTES[m] + "-major"
            else:
                CHORD = "other"
            break
        else:
            CHORD = "other"
    OUTPUT += CHORD + " "

print OUTPUT

# pylint: disable=pointless-string-statement
'''
$ python badwolf10.py
3
87 73 64 61
46 37 53 58 70
44 48 51 56 32
other A#-minor G#-major
'''
