#=
 $julia
 julia> using Lint
 julia> lintfile("kergrau.jl")
=#

using StatsBase
open("DATA.lst") do file
  counter = 0
  match_word = String[]
  for ln in eachline(file)
    for i in split(ln, " ")
      a_words = split(ln, " ")
      occu = countmap(a_words)
      if occu["$i"] >= 2
        push!(match_word,  i)
      end
    end
    match_word = unique(match_word)
    sort!(match_word, lt=lexless)
    for i in match_word
      println(i)
    end
  end
end

# $ kergrau.jl
# bec bes bip bop boq box byx daq deh dih dik diq dix dyh dyk gaf gat gex gih
# giq gix gok gut gyf gyh juk jus jut jyh jyq jys lax lif lih lok lot luh mah
# meq mes mif muq mut myq nit nuf rac raq roc rop ruc ruf vyh zep zes zyk zyx
