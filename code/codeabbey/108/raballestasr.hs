-- | Codeabbey 108: Star medals
-- | http://www.codeabbey.com/index/task_view/star-medals
-- | **** Compilation
-- | $ ghc -dynamic raballestasr.hs
-- | [1 of 1] Compiling Main             ( raballestasr.hs, raballestasr.o )
-- | Linking raballestasr ...
-- | **** Test run (with hard DATA.lst)
-- | $ ./raballestasr
-- | "20 51 27 102 32 57 15 11 17"
-- | **** Linter output
-- | $ hlint raballestasr.hs
-- | Found:
-- |   b !! 0
-- | Why not:
-- |   head b
-- | 1 hint
-- | **** Ignored for consistency (b!!1 used in same line)
-- | ****
-- | Every one of the N rays crosses 2*(step-1) other rays.
-- | If you count like this you get 2*(step-1)*rays but you 
-- | counted every intersection twice. Divide by 2 so the
-- | number of rays is simply rays*(step-1).

-- | Read codeabbey usual test cases.
main = do
    contents <- readFile "DATA.lst"
    let input = tail $ lines contents
    let a = map words input
    let pf = map (\b -> (read (b!!0) ::Int)*((read (b!!1) ::Int)-1))
    print $ unwords $ map show (pf a)
