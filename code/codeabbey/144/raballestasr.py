#!/usr/bin/python3
""" This script solves the equation a*x + b = 0 (mod m)
for a number of test cases.
"""

def extended_euclid(num1, num2):
    """ Performs the extended Euclidean algorithm on the
    given numbers and returns a list [g, a, b] such that
    a*num1 + b*num2 = g = gcd(num1, num2).
    """
    s_cur, t_cur, s_pre, t_pre = 0, 1, 1, 0
    r_cur, r_pre = num2, num1
    while r_cur:
        quo = r_pre // r_cur
        r_pre, r_cur = r_cur, r_pre - quo*r_cur
        s_pre, s_cur = s_cur, s_pre - quo*s_cur
        t_pre, t_cur = t_cur, t_pre - quo*t_cur
    return [r_pre, s_pre, t_pre]

def inverse_modulo(res, mod):
    """ Returns the inverse modulo mod of the number res
    or -1 if the number is not invertible.
    """
    dcg, inv = extended_euclid(res, mod)[0:2]
    aus = None
    if dcg == 1:
        if inv < 0:
            aus = inv + mod
        else:
            aus = inv
    return aus

def solve_linear_equation_modulo(mod, coef, const):
    """ Solves the equation coef*x + const = 0 modulo mod
    if possible. If not, returns -1.
    """
    inv = inverse_modulo(coef, mod)
    aus = -1
    if inv is not None:
        aus = (-const*inv) % mod
    return aus

F = open("DATA.lst", "r")
L = int(F.readline())
AUS = ""
for l in range(L):
    mod_t, coef_t, const_t = list(map(int, F.readline().split()))
    AUS += str(solve_linear_equation_modulo(mod_t, coef_t, const_t)) + " "
print(AUS)
F.close()
#
# pylint raballestasr.py 
# No config file found, using default configuration
# ************* Module raballestasr
# C: 50, 0: Final newline missing (missing-final-newline)
#
# ------------------------------------------------------------------
# Your code has been rated at 9.68/10 (previous run: 9.35/10, +0.32)
