## Version 2.0
## language: en

Feature: HEEEEEEERE'S-Johnny!-Cryptography-2018game-picoctf
  Code:
    HEEEEEEERE'S-Johnny!
  Site:
    2018game-picoctf
  Category:
    Cryptography
  User:
    rferi1894
  Goal:
    Crack the files, get info and catch the flag.

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | Ubuntu          | 18.04.2 LTS |
    | Google Chrome   | 74.0.3729   |

  Machine information:
    Given the challenge URL
    """
    https://2018game.picoctf.com/problems
    """
    And the challenge information
    """
    Okay, so we found some important looking files on a linux computer.
    Maybe they can be used to get a password to the process. Connect
    with nc 2018shell.picoctf.com 38860.
    """"
    And the files mentioned
    """"
    passwd shadow
    """"
    And the field to submit the flag

  Scenario: Success:John the Ripper
    Given the files I can work with
    Then I research cracking methods
    And I discover the unshadow command
    And also the John command
    Then I use unshadow on the files
    And save the output to a new file
    """
    $ unshadow passwd shadow > crack
    """
    Then I use the John command
    And a good dictionary file
    """
    $ john --wordlist=rockyou.txt crack
    Loaded 1 password hash (crypt, generic crypt(3) [?/64])
    No password hashes left to crack (see FAQ)
    """
    And look at the cracked password
    """"
    $ john -show crack
    root:password1:0:0:root:/root:/bin/bash

    1 password hash cracked, 0 left
    """
    Then I use the user and password to catch the flag
    And I solve the challenge
