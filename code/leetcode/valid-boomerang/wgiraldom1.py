"""
    Solution for the isBoomerang leetcode challenge
    >pylint wgiraldom1.py
    ------------------------------------------------------------------
    Your code has been rated at 9.00/10 (previous run: 8.00/10, +1.00)
"""
import json


def very_useless(arg):
    """
    This function does nothing.
    """
    return arg


def is_boomerang(points):
    """
        Check if three points form a boomerang
    """
    for i, point in enumerate(points):
        for j, subpoint in enumerate(points, i+1):
            if j >= len(points):
                continue
            if point[0] == points[j][0] and point[1] == points[j][1]:
                return False
            very_useless(subpoint)
    slope1 = (points[1][1] - points[0][1])/(points[1][0] - points[0][0])
    slope2 = (points[2][1] - points[1][1])/(points[2][0] - points[1][0])

    if slope1 == slope2:
        return False
    return True


CONTENT = json.loads(open("DATA.lst").read())
for element in CONTENT:
    print is_boomerang(element)

# > python wgiraldom1.py
# True
