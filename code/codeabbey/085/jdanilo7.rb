# $ rubocop jdanilo7.rb
# Inspecting 1 file

# 1 file inspected, no offenses detected

def read_lines_file(filename)
  lines = []
  File.foreach(filename) do |line|
    lines.push(line.split(' '))
  end
  lines
end

def rotate_point(xcoor, ycoor, angle)
  cos_ang = Math.cos(angle)
  sin_ang = Math.sin(angle)
  xnew = xcoor * cos_ang - ycoor * sin_ang
  ynew = ycoor * cos_ang + xcoor * sin_ang
  [xnew.round, ynew.round]
end

def sort_stars(stars)
  stars.sort { |a, b| [a[2], a[1]] <=> [b[2], b[1]] }
end

def rotate_stars(stars, angle)
  stars.each do |star|
    star[1], star[2] = rotate_point(star[1].to_i, star[2].to_i, angle)
  end
  stars
end

def process_input(stars, angle)
  final_stars = ''
  stars = sort_stars(rotate_stars(stars, angle))
  stars.each do |star|
    final_stars += star[0] + ' '
  end
  final_stars
end

stars = read_lines_file('DATA.lst')
angle = stars[0][1].to_i
angle = angle * Math::PI / 180
stars.shift

puts process_input(stars, angle)

# $ ruby jdanilo7.rb
# Algol Deneb Thabit Aldebaran Mira Sirius Kastra Heka Electra Capella Gemma
# Rigel Mizar Albireo Yildun Polaris Unuk Altair Media Fomalhaut Zosma Diadem
# Betelgeuse Jabbah Lesath Nembus Pherkad Procyon Alcyone Bellatrix Castor
# Alcor Kochab Vega
