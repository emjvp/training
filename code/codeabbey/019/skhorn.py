#!/usr/bin/env python3
"""
Problem #19 Matching Brackets
"""
import pdb

class MatchingBrackets:

    output = []
    def __init__(self):

        while True:

            line = input()

            if line:

                if len(line) < 3:
                    pass
                else:
                    data = self.separate_line(line)
                    print(data)
                    self.search_for_match(data)

            else:
                break

        text = ' '.join(self.output)
        print(text)

    """
    Separar caracter a caracter y solo agregar los
    que cumplan con ()[]{}<>
    """
    def separate_line(self, data):
        #pdb.set_trace()
        new_data = []

        for i, item in enumerate(data):

            if item in "()" or \
               item in "[]" or \
               item in "{}" or \
               item in "<>":

                new_data.append(item)

        return new_data

    def search_for_match(self, data):

        #pdb.set_trace()
        spec_char = []
        temp = ""
        for i, item in enumerate(data):

            if item is "(" or\
               item is "[" or\
               item is "<" or\
               item is "{":

                spec_char.append(item)
                temp = item

            elif temp is "(" and item is ")":
                spec_char.pop()
                temp = ""

            elif temp is "[" and item is "]":
                spec_char.pop()
                temp = ""

            elif temp is "<" and item is ">":
                spec_char.pop()
                temp = ""

            elif temp is "{" and item is "}":
                spec_char.pop()
                temp = ""
            else:
                spec_char.append(item)

        if len(spec_char) > 0:

            if ")" not in spec_char and\
               "]" not in spec_char and\
               "}" not in spec_char and\
               ">" not in spec_char:

                self.output.append("0")
            else:
                if len(data) == len(spec_char):
                    flag = self.compare(data, spec_char)
                    if not flag:
                        self.output.append("0")
                    else:
                        self.search_for_match(spec_char)
                else:
                    self.search_for_match(spec_char)

        elif not spec_char:
            self.output.append("1")

    def compare(self, list1, list2):

        for i in range(len(list1)):
            if list1[i] == list2[i]:
                pass
            else:
                return False

MatchingBrackets()
