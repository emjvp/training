/*
With JavaScript - Node.js
Linting:   eslint jenniferagve.js
--------------------------------------------------------------------
✖ 0 problem (0 error, 0 warnings)
*/

function sortProcess(numberSec) {
  const inputSegment = numberSec.split(' ');
  const numberConvert = inputSegment.map((element) => Number(element));
  const minFind = Math.min(...numberConvert);
  const maxFind = Math.max(...numberConvert);
  const median = numberConvert.filter((element) => ((element !== minFind) &&
    (element !== maxFind)));
  const medianFormat = Number(median[0]);
  const output = process.stdout.write(`${ medianFormat } `);
  return output;
}

function dataProcess(erro, contents) {
  if (erro) {
    return erro;
  }
  const inputFile = contents.split('\n');
  const inputSequence = inputFile.slice(1);
  const eachSequence = inputSequence.map((numberSec) => sortProcess(
    numberSec));
  return eachSequence;
}

const filesRe = require('fs');
function fileLoad() {
/* Load of the file to read it*/
  return filesRe.readFile('DATA.lst', 'utf8', (erro, contents) =>
    dataProcess(erro, contents));
}
/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
node jenniferagve.js
input:3
7 3 5
15 20 40
300 550 137
-------------------------------------------------------------------
output:
5 20 300
*/
