﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleChallange
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            Int64 diven;
            double divble;
            String[,] val = new String[n, 2];
            String Line;
            String[] lspl;
            for (int i = 0; i < n; i++)
            {
                Line = Console.ReadLine();
                lspl = Line.Split(' ');
                for (int j = 0; j < 2; j++)
                {
                    val[i, j] = lspl[j];

                }
            }
            for (int k = 0; k < n; k++)
            {
                diven = Int64.Parse(val[k, 0]) / Int64.Parse(val[k, 1]);
                divble = Double.Parse(val[k, 0]) / Double.Parse(val[k, 1]);
                if (diven < 0)
                {
                    diven = diven * -1;
                    divble = divble * -1;
                    if (divble - diven >= 0.5)
                    {
                        diven++;
                    }
                    diven = diven * -1;
                }
                else
                {
                    if (divble - diven >= 0.5)
                    {
                        diven++;
                    }
                }
                Console.Write(diven + " ");
            }
            Console.ReadKey(true);
        }
    }
}
