" ""My solutions First line"""
# #Tic Tac Toe GAME python 2.7
# #the tic tac toe
# #is now programmed in python 2.7
# #a game to have fun and think about our next move
# #pylint cd\...\shalo10967.py
# #rate 10.0/10
from turtle import Turtle, Screen
TIMMY = Turtle()
SCREEN = Screen()
SCREEN.setup(600, 600, 400, 20)
SCREEN.title("TIC TAC TOE")
TIMMY.hideturtle()
# #lines tictact
TIMMY.pensize(5)
TIMMY.penup()
TIMMY.goto(100, 230)
TIMMY.pendown()
TIMMY.goto(100, -230)
TIMMY.penup()
TIMMY.goto(-100, 230)
TIMMY.pendown()
TIMMY.goto(-100, -230)
# #lines horiz
TIMMY.penup()
TIMMY.goto(-230, -70)
TIMMY.pendown()
TIMMY.goto(230, -70)
TIMMY.penup()
TIMMY.goto(-230, 70)
TIMMY.pendown()
TIMMY.goto(230, 70)
# #texts
TIMMY.penup()
M = - 150
Y = 100
# #active
ES1 = 0
ES2 = 0
ES3 = 0
ES4 = 0
ES5 = 0
ES6 = 0
ES7 = 0
ES8 = 0
ES9 = 0
POSX1 = ''
POSX2 = ''
POSX3 = ''
POSX4 = ''
POSX5 = ''
POSX6 = ''
POSX7 = ''
POSX8 = ''
POSX9 = ''
POSO1 = ''
POSO2 = ''
POSO3 = ''
POSO4 = ''
POSO5 = ''
POSO6 = ''
POSO7 = ''
POSO8 = ''
POSX = 0
POSO9 = ''
D = 0
for i in range(1, 10):
    TIMMY.goto(M, Y)
    TIMMY.write(i, False, "right", ("arial", 20, "bold italic"))
    M += 150
    if i % 3 == 0:
        Y -= 120
        M = - 150
# #commands
TIMMY.penup()
TIMMY.goto(120, -270)
TIMMY.pencolor("brown")
TIMMY.write("Abre la consola de coMandos, y digita la POSicion:[1...9] ",
            False, "right", ("arial", 12, "bold italic"))
for j in range(1, 10):
    if j % 2 != 0:
        with open("DATA.lst", "r") as F:
            DATOS = F.readlines()
            for LINEA in DATOS:
                LISTA = LINEA.rstrip('\n').split("\t")
                for CELS in LISTA:
                    D = D + 1
                    if D % 2 != 0:
                        POSX = int(CELS)
                        print "TURNO: ", D, "POSX ", POSX
                        POS = POSX
                        if(POS == 1 and ES1 == 0):
                            TIMMY.penup()
                            TIMMY.goto(-120, 50)
                            TIMMY.pencolor("brown")
                            TIMMY.write("X", False,
                                        "right",
                                        ("arial", 80, "bold italic"))
                            ES1 = 1
                            POSX1 = 'x'
                        if(POS == 2 and ES2 == 0):
                            TIMMY.penup()
                            TIMMY.goto(30, 50)
                            TIMMY.pencolor("brown")
                            TIMMY.write("X", False, "right",
                                        ("arial", 80, "bold italic"))
                            ES2 = 1
                            POSX2 = 'x'
                        if(POS == 3 and ES3 == 0):
                            TIMMY.penup()
                            TIMMY.goto(180, 50)
                            TIMMY.pencolor("brown")
                            TIMMY.write("X", False, "right",
                                        ("arial", 80, "bold italic"))
                            ES3 = 1
                            POSX3 = 'x'
                        if(POS == 4 and ES4 == 0):
                            TIMMY.penup()
                            TIMMY.goto(-120, -70)
                            TIMMY.pencolor("brown")
                            TIMMY.write("X", False, "right",
                                        ("arial", 80, "bold italic"))
                            ES4 = 1
                            POSX4 = 'x'
                        if(POS == 5 and ES5 == 0):
                            TIMMY.penup()
                            TIMMY.goto(30, -70)
                            TIMMY.pencolor("brown")
                            TIMMY.write("X", False, "right",
                                        ("arial", 80, "bold italic"))
                            ES5 = 1
                            POSX5 = 'x'
                        if(POS == 6 and ES6 == 0):
                            TIMMY.penup()
                            TIMMY.goto(180, -70)
                            TIMMY.pencolor("brown")
                            TIMMY.write("X", False, "right",
                                        ("arial", 80, "bold italic"))
                            ES6 = 1
                            POSX6 = 'x'
                        if(POS == 7 and ES7 == 0):
                            TIMMY.penup()
                            TIMMY.goto(-120, -190)
                            TIMMY.pencolor("brown")
                            TIMMY.write("X", False, "right",
                                        ("arial", 80, "bold italic"))
                            ES7 = 1
                            POSX7 = 'x'
                        if(POS == 8 and ES8 == 0):
                            TIMMY.penup()
                            TIMMY.goto(30, -190)
                            TIMMY.pencolor("brown")
                            TIMMY.write("X", False, "right",
                                        ("arial", 80, "bold italic"))
                            ES8 = 1
                            POSX8 = 'x'
                        if(POS == 9 and ES9 == 0):
                            TIMMY.penup()
                            TIMMY.goto(180, -190)
                            TIMMY.pencolor("brown")
                            TIMMY.write("X", False, "right",
                                        ("arial", 80, "bold italic"))
                            ES9 = 1
                            POSX9 = 'x'
                    if D % 2 == 0:
                        POSO = int(CELS)
                        print "TURNO: ", D, "POSO ", POSO
                        POS = POSO
                        if(POS == 1 and ES1 == 0):
                            TIMMY.penup()
                            TIMMY.goto(-120, 50)
                            TIMMY.pencolor("blue")
                            TIMMY.write("O", False, "right",
                                        ("arial", 80, "bold italic"))
                            ES1 = 1
                            POSO1 = 'o'
                        if(POS == 2 and ES2 == 0):
                            TIMMY.penup()
                            TIMMY.goto(30, 50)
                            TIMMY.pencolor("blue")
                            TIMMY.write("O", False, "right",
                                        ("arial", 80, "bold italic"))
                            ES2 = 1
                            POSO2 = 'o'
                        if(POS == 3 and ES3 == 0):
                            TIMMY.penup()
                            TIMMY.goto(180, 50)
                            TIMMY.pencolor("blue")
                            TIMMY.write("O", False, "right",
                                        ("arial", 80, "bold italic"))
                            ES3 = 1
                            POSO3 = 'o'
                        if(POS == 4 and ES4 == 0):
                            TIMMY.penup()
                            TIMMY.goto(-120, -70)
                            TIMMY.pencolor("blue")
                            TIMMY.write("O", False, "right",
                                        ("arial", 80, "bold italic"))
                            ES4 = 1
                            POSO4 = 'o'
                        if(POS == 5 and ES5 == 0):
                            TIMMY.penup()
                            TIMMY.goto(30, -70)
                            TIMMY.pencolor("blue")
                            TIMMY.write("O", False, "right",
                                        ("arial", 80, "bold italic"))
                            ES5 = 1
                            POSO5 = 'o'
                        if(POS == 6 and ES6 == 0):
                            TIMMY.penup()
                            TIMMY.goto(180, -70)
                            TIMMY.pencolor("blue")
                            TIMMY.write("O", False, "right",
                                        ("arial", 80, "bold italic"))
                            ES6 = 1
                            POSO6 = 'o'
                        if(POS == 7 and ES7 == 0):
                            TIMMY.penup()
                            TIMMY.goto(-120, -190)
                            TIMMY.pencolor("blue")
                            TIMMY.write("O", False, "right",
                                        ("arial", 80, "bold italic"))
                            ES7 = 1
                            POSO7 = 'o'
                        if(POS == 8 and ES8 == 0):
                            TIMMY.penup()
                            TIMMY.goto(30, -190)
                            TIMMY.pencolor("blue")
                            TIMMY.write("O", False, "right",
                                        ("arial", 80, "bold italic"))
                            ES8 = 1
                            POSO8 = 'o'
                        if(POS == 9 and ES9 == 0):
                            TIMMY.penup()
                            TIMMY.goto(180, -190)
                            TIMMY.pencolor("blue")
                            TIMMY.write("O", False, "right",
                                        ("arial", 80, "bold italic"))
                            ES9 = 1
                            POSO9 = 'o'
                    if(POSX3 == 'x' and POSX5 == 'x' and POSX7 == 'x'):
                        TIMMY.goto(-10, -50)
                        TIMMY.pencolor("green")
                        TIMMY.write("GANA X!", False, "center",
                                    ("arial", 100, "bold italic"))
                        TIMMY.goto(0, -285)
                        TIMMY.write("Click derecho para cerrar",
                                    False, "right",
                                    ("arial", 6, "bold italic"))
                        SCREEN.exitonclick()
                    if(POSX9 == 'x' and POSX8 == 'x' and POSX7 == 'x'):
                        TIMMY.goto(-10, -50)
                        TIMMY.pencolor("green")
                        TIMMY.write("GANA X!", False, "center",
                                    ("arial", 100, "bold italic"))
                        TIMMY.goto(0, -285)
                        TIMMY.write("Click derecho para cerrar",
                                    False, "right",
                                    ("arial", 6, "bold italic"))
                        SCREEN.exitonclick()
                    if(POSX4 == 'x' and POSX5 == 'x' and POSX6 == 'x'):
                        TIMMY.goto(-10, -50)
                        TIMMY.pencolor("green")
                        TIMMY.write("GANA X!", False, "center",
                                    ("arial", 100, "bold italic"))
                        TIMMY.goto(0, -285)
                        TIMMY.write("Click derecho para cerrar",
                                    False, "right",
                                    ("arial", 6, "bold italic"))
                        SCREEN.exitonclick()
                    if(POSX1 == 'x' and POSX2 == 'x' and POSX3 == 'x'):
                        TIMMY.goto(-10, -50)
                        TIMMY.pencolor("green")
                        TIMMY.write("GANA X!", False, "center",
                                    ("arial", 100, "bold italic"))
                        TIMMY.goto(0, -285)
                        TIMMY.write("Click derecho para cerrar",
                                    False, "right",
                                    ("arial", 6, "bold italic"))
                        SCREEN.exitonclick()
                    if(POSX1 == 'x' and POSX4 == 'x' and POSX7 == 'x'):
                        TIMMY.goto(-10, -50)
                        TIMMY.pencolor("green")
                        TIMMY.write("GANA X!", False, "center",
                                    ("arial", 100, "bold italic"))
                        TIMMY.goto(0, -285)
                        TIMMY.write("Click derecho para cerrar",
                                    False, "right",
                                    ("arial", 6, "bold italic"))
                        SCREEN.exitonclick()
                    if(POSX2 == 'x' and POSX5 == 'x' and POSX8 == 'x'):
                        TIMMY.goto(-10, -50)
                        TIMMY.pencolor("green")
                        TIMMY.write("GANA X!", False, "center",
                                    ("arial", 100, "bold italic"))
                        TIMMY.goto(0, -285)
                        TIMMY.write("Click derecho para cerrar",
                                    False, "right",
                                    ("arial", 6, "bold italic"))
                        SCREEN.exitonclick()
                    if(POSX3 == 'x' and POSX6 == 'x' and POSX9 == 'x'):
                        TIMMY.goto(-10, -50)
                        TIMMY.pencolor("green")
                        TIMMY.write("GANA X!", False, "center",
                                    ("arial", 100, "bold italic"))
                        TIMMY.goto(0, -285)
                        TIMMY.write("Click derecho para cerrar",
                                    False, "right",
                                    ("arial", 6, "bold italic"))
                        SCREEN.exitonclick()
                    if(POSX1 == 'x' and POSX5 == 'x' and POSX9 == 'x'):
                        TIMMY.goto(-10, -50)
                        TIMMY.pencolor("green")
                        TIMMY.write("GANA X!", False, "center",
                                    ("arial", 100, "bold italic"))
                        TIMMY.goto(0, -285)
                        TIMMY.write("Click derecho para cerrar",
                                    False, "right",
                                    ("arial", 6, "bold italic"))
                        SCREEN.exitonclick()
                    if(POSO9 == 'o' and POSO8 == 'o' and POSO7 == 'o'):
                        TIMMY.goto(-10, -50)
                        TIMMY.pencolor("red")
                        TIMMY.write("GANA O!", False, "center",
                                    ("arial", 100, "bold italic"))
                        TIMMY.goto(0, -285)
                        TIMMY.write("Click derecho para cerrar",
                                    False, "right",
                                    ("arial", 6, "bold italic"))
                        SCREEN.exitonclick()
                    if(POSO4 == 'o' and POSO5 == 'o' and POSO6 == 'o'):
                        TIMMY.goto(-10, -50)
                        TIMMY.pencolor("red")
                        TIMMY.write("GANA O!", False, "center",
                                    ("arial", 100, "bold italic"))
                        TIMMY.goto(0, -285)
                        TIMMY.write("Click derecho para cerrar",
                                    False, "right",
                                    ("arial", 6, "bold italic"))
                        SCREEN.exitonclick()
                    if(POSO1 == 'o' and POSO2 == 'o' and POSO3 == 'o'):
                        TIMMY.goto(-10, -50)
                        TIMMY.pencolor("red")
                        TIMMY.write("GANA O!", False, "center",
                                    ("arial", 100, "bold italic"))
                        TIMMY.goto(0, -285)
                        TIMMY.write("Click derecho para cerrar",
                                    False, "right",
                                    ("arial", 6, "bold italic"))
                        SCREEN.exitonclick()
                    if(POSO1 == 'o' and POSO4 == 'o' and POSO7 == 'o'):
                        TIMMY.goto(-10, -50)
                        TIMMY.pencolor("red")
                        TIMMY.write("GANA O!", False, "center",
                                    ("arial", 100, "bold italic"))
                        TIMMY.goto(0, -285)
                        TIMMY.write("Click derecho para cerrar",
                                    False, "right",
                                    ("arial", 6, "bold italic"))
                        SCREEN.exitonclick()
                    if(POSO2 == 'o' and POSO5 == 'o' and POSO8 == 'o'):
                        TIMMY.goto(-10, -50)
                        TIMMY.pencolor("red")
                        TIMMY.write("GANA O!", False, "center",
                                    ("arial", 100, "bold italic"))
                        TIMMY.goto(0, -285)
                        TIMMY.write("Click derecho para cerrar",
                                    False, "right",
                                    ("arial", 6, "bold italic"))
                        SCREEN.exitonclick()
                    if(POSO3 == 'o' and POSO6 == 'o' and POSO9 == 'o'):
                        TIMMY.goto(-10, -50)
                        TIMMY.pencolor("red")
                        TIMMY.write("GANA O!", False, "center",
                                    ("arial", 100, "bold italic"))
                        TIMMY.goto(0, -285)
                        TIMMY.write("Click derecho para cerrar",
                                    False, "right",
                                    ("arial", 6, "bold italic"))
                        SCREEN.exitonclick()
                    if(POSO1 == 'o' and POSO5 == 'o' and POSO9 == 'o'):
                        TIMMY.goto(-10, -50)
                        TIMMY.pencolor("red")
                        TIMMY.write("GANA O!", False, "center",
                                    ("arial", 100, "bold italic"))
                        TIMMY.goto(0, -285)
                        TIMMY.write("Click derecho para cerrar",
                                    False, "right",
                                    ("arial", 6, "bold italic"))
                        SCREEN.exitonclick()
                    if(POSO3 == 'o' and POSO5 == 'o' and POSO7 == 'o'):
                        TIMMY.goto(-10, -50)
                        TIMMY.pencolor("red")
                        TIMMY.write("GANA O!", False, "center",
                                    ("arial", 100, "bold italic"))
                        TIMMY.goto(0, -285)
                        TIMMY.write("Click derecho para cerrar",
                                    False, "right",
                                    ("arial", 6, "bold italic"))
                        SCREEN.exitonclick()
# #close program
TIMMY.penup()
TIMMY.goto(0, -285)
TIMMY.write("Click derecho para cerrar",
            False, "right", ("arial", 6, "bold italic"))
SCREEN.exitonclick()
# #My solutions last line
# #python cd\...\shalo10967.py
# #solve turn 8
