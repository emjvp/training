#include <iostream>

using namespace std;

void FWG(int num,long int X0,long int *len){
    long int Xcurr=X0;
    for(int i=0;i<num;i++){
        int A=445;
        long int C=700001,M=2097152;
        char cons[20]="bcdfghjklmnprstvwxz", vow[6]="aeiou";
        for(int j=0;j<len[i];j++){
            char word[len[i]+1];
            long int X1;
            X1=(A*Xcurr+C)%M;
            Xcurr=X1;
            if(j%2==1){
                word[j]=vow[X1%5];
            }
            else{
                word[j]=cons[X1%19];
            }
            if(j==len[i]-1){
                word[j+1]='\0';
                cout<<word<<' ';
            }
        }
    }
}

int main(void){
    int num;
    long int X0;
    cin>>num>>X0;
    long int len[num];
    for(int i=0;i<num;i++){
        cin>>len[i];
    }
    FWG(num,X0,&len[0]);
    return 0;
}
