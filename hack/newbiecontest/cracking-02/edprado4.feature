Feature: Solve Hello, World challenge
  from site Newbiecontest
  logged as EdPrado4

Background:
  Given I have access to Internet
  And I have Ubuntu 16.04 LTS OS
  And I have VMWare player software
  And I have a Windows XP virtual machine
  And I have OllyDbg software

Scenario: Challenge solved
  Given An executable file with password
  When I load it into OllyDbg
  And I inspect the core module of the program
  When I read the assembler code
  Then I notice an user defined string
  When I use that string as password
  Then I can access the program
  And I get the keyword to solve the challenge
