#!/usr/bin/env python
'''
$ pylint badwolf10.py #linting
No config file found, using default configuration
--------------------------------------------------------------------
Your code has been rated at 10.00/10 (previous run: 10.00/10, +0.00)
$ python badwolf10.py #compilation
'''

import math

FILE = open("DATA.lst", "r")
for line in FILE:
    TUNES = [float(x) for x in line.split()]

NOTES = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']
OUTPUT = ""
C0 = 16.351597831287375
GAMMA = math.pow(2.0, 1.0/12.0)

for tune in TUNES:
    n = int(round(math.log(tune/C0)/math.log(GAMMA)))
    note = NOTES[n % 12] + str(int(math.floor(n / 12)))
    OUTPUT += note + " "

print OUTPUT
# pylint: disable=pointless-string-statement
'''
$ python badwolf10.py
F#3 A#2 G#3 A1 G#1 F2 A#5 A#3 B1 C2 F#4 G5
'''
