#Modular calculator
data=[x.split() for x in """668
+ 858
* 56
+ 2
+ 1423
+ 51
* 1
* 839
* 71
+ 892
* 587
* 20
+ 5
+ 75
+ 70
* 9932
* 2176
* 9279
* 3
+ 847
* 58
+ 9694
* 977
+ 558
+ 98
+ 93
+ 695
+ 5
+ 63
+ 26
* 69
* 4394
* 370
* 40
* 460
+ 8
+ 4837
* 663
+ 15
+ 65
+ 1
* 937
* 5116
+ 24
+ 86
+ 7
* 87
+ 4548
% 7261
""".splitlines()]

def ModCal(args):
  result=int(args[0][0])
  for i in range(1,len(args)):
    if args[i][0]=='+':
      result=result+int(args[i][1])
    elif args[i][0]=='*':
      result=result*int(args[i][1])
    elif args[i][0]=='%':
      result=result%int(args[i][1])
  print(result)

ModCal(data)
