"""
Given a certain array, find out if it's a permutation
"""
# !/usr/bin/python
# [Finished in 0.0s]

# pylint edr3mo.py
# No config file found, using default configuration
#
# ---------------------------------------------------------------------
# Your code has been rated at 10.00/10 (previous run: -1.11/10, +11.11)


def is_permutation(number, input_array):
    """find out if input_array is a permutation of number"""
    permutation = False
    for i in range(1, number+1):
        if i in input_array:
            permutation = True
        else:
            permutation = False
            break

    print permutation
# python ./edr3mo.py
# True
