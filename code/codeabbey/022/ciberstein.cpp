/*
Linting with CppCheck assuming the #include files are on the same
folder as ciberstein.cpp
$ cppcheck --enable=all --inconclusive --std=c++14 ciberstein.cpp
Checking ciberstein.cpp ...

$ g++.exe "C:\\...\ciberstein.cpp" -o "C:\\...\ciberstein.exe"
  -I"C:\\...\include" -I"C:\\...\include" -I"C:\\...\include"
  -I"C:\\...\c++" -L"C:\\...\lib" -static-libgcc

Compiling and linking using the "Dev-C++ 5.11"

Compilation results...
--------
- Errors: 0
- Warnings: 0
- Output Filename: C:\\...\ciberstein.exe
- Output Size: 1,83309745788

/out:ciberstein.exe

*/

#include <iostream>
#include <fstream>

using namespace std;

int main() {

  int nc, x, y, n;
  int A1, A2, B1, B2, R;

  ifstream fin("DATA.lst");

  if(fin.fail())
    cout<<"Error DATA.lst not found";

  else {
    fin>>nc;
    int answer[nc];

    for(int i = 0 ; i < nc ; i++) {

     fin>>x>>y>>n;

     A1 = (y*n / (x+y));
     A2 = (x*n / (x+y));

     if ((A1+1)*x > A2*y)
      B1 = (A1+1)*x;

     else
      B1 = A2*y;

     if (A1*x > (A2+1)*y)
      B2 = A1*x;

     else
      B2 = (A2+1)*y;

     if (B1 < B2)
      R = B1;

     else
      R = B2;

     answer[i]=R;
    }

    cout<<"answer:"<<endl;
    for(int i = 0 ; i < nc ; i++)
      cout<<answer[i]<<" ";
  }
}
/*
$ ciberstein.exe
318244719 13158054 278833660 132435810 51272717 46729010 8760609 107074208
309055713 33295378 322580664 359769725 332892224 162592318 175123025
*/
