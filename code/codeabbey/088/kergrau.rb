# $ruby-lint kergrau.rb

answer = ''
flag = false
octave = ['C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']

# open file code and itaration I take of
File.open('DATA.lst', 'r') do |file|
  while line = file.gets
    unless flag
      flag = true
      next
    end

    line.split(' ').each do |note|
      a = 55
      distance = 0

      if note.length == 2
        distance = octave.index(note[0]) - octave.index('A')
      else
        var = octave.index(note[0..1])
        distance = var.to_i - octave.index('A')
      end
       i = 0
      while i < note[-1].to_i - 1
        a = a * 2
        i += 1
      end

      frecuency = a * 1.059463094 ** distance
      answer << "#{frecuency.round} "
    end

  end
end
puts answer

# ruby kergrau.rb
# 831 82 392 58 37 35 740 98 139 659 69 587 33
# 208 466 156 196 104 87 988 233 415
