#!/usr/bin/env ruby
# $ ruby -wc jicardona.rb
# Syntax OK

total = 0
testCases = []

File.open('DATA.lst') do |file|
  total = file.readline
  file.each_line { |line| testCases.push(line.strip) }
end

solution = []

testCases.each do |testCase|
  lower = lSwap = 0
  testCase.each_char.with_index do |char, index|
    lower = index
    for position in index + 1..testCase.length - 1
      if testCase[position] < char && testCase[position] <= testCase[lower]
        if index != 0 || testCase[position] != '0'
          lower = position
          lSwap = index
        end
      end
    end
    if lower != index
      break
    end
  end
  upper = uSwap = 0
  testCase.each_char.with_index do |char, index|
    upper = index
    for position in index + 1..testCase.length - 1
      if testCase[position] > char && testCase[position] >= testCase[upper]
        upper = position
        uSwap = index
      end
    end
    if upper != index
      break
    end
  end
  min = testCase.clone
  min[lSwap], min[lower] = min[lower], min[lSwap]
  max = testCase.clone
  max[uSwap], max[upper] = max[upper], max[uSwap]
  solution.push(min, max)
end

puts solution.join(' ')

# $ ruby jicardona.rb
# 1B4906963D2C0E66C70CF        FB4906963D2C0E16C70C6
# 1B880619E752368ACB7E78D9F    FB880619E752168ACB7E78D93
# 1F50A7C30DA2689CBB245FF      FF50A7C30DA1689CBB245F2
# 175B14434A4F2032ED356DE2836  F75B14434A4E2032ED356D12836
# 16B0E3C0503AA7AC8DE6B2B20C48 E6B0E1C0503AA7AC8D36B2B20C48
# 163B2303F0903                F6312303B0903
# 1AE72B69794A9BF22            FAE72B69794A91B22
# 35CB8CE8C890C4BACE697DCC     E5CB8CE8C890C4BAC9637DCC
# 1D69F124DEDAD675F7742        FD69F124DEDAD67577142
# 10048F14672D11F36D247FBE4    F0148F04672D11F36D2471BE4
# 15F90092411568D85EA427245    F5790092411568D85EA421245
# 1187CFDC8DD2F1347A942E3C     F187CFDC8DD241317A942E3C
# 1D693FD484186E0FB            FD693FD484181E06B
# 1D60409938CFCFD              FF60409938CDC1D
# 11286BD83F63EB6              F1286BD83B63E16
# 1CE3D8D0090DBEF6FFF6CCEFB439 FFE3D8D0090DBEF6F1F6CCECB439
# 2856693E64F04DB3C29E         F856693E64904DB3C22E
# 2BD7B8CE937A9E               EBD7B8CE237A99
# 130950AAD5D970               D10950AAD53970
# 1A369EFDC204C92EC4E6AE       FA361E9DC204C92EC4E6AE
