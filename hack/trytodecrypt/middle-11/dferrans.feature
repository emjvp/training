## Version 2.0
## language: en

Feature: middle-11-decrypt-trytodecrypt.com
  Code:
    middle-11
  Site:
    trytodecrypt.com
  Category:
    decrypt
  User:
    dferrans
  Goal:
    decrypt secret string

  Background:
  Hacker's software:
    | <Software name> | <Version>   |
    | ubuntu          | 18.04.2     |
    | firefox         | 67.0(x64)   |
    | selenium        | 3.14.0      |
    | python          | 3.6.7       |

  Machine information:
    Given the challenge URL
    """
    https://www.trytodecrypt.com/decrypt.php?id=11#headline
    """
    And the string to decrypt:
    """
    3785824AD56B2531A7150DF44C21434A61E63F040A42F2012BC2F43F
    0AD535D24D46013213866D7E0
    """

  Scenario: Success:brute-force-encription-with-selenium-python-script
    When I access the challenge using selenium and firefox
    And I start and automated python script to match allowed words
    And I use the most common english letters in words.
    And I run every posible combination to match every character individually
    Then I am able to see how the script start to match the secret word
    Then I wait some minutes for the script to finish.
    Then I get the string that solves the problem file[evidence](solved.png)
