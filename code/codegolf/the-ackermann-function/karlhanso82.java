/*
$pmd.bat -d karlhanso82.java -R rulesets/java/quickstart.xml -f text #linting
may 15, 2019 9:35:03 AM net.sourceforge.pmd.PMD processFiles
ADVERTENCIA:
This analysis could be faster, please consider using
Incremental  Analysis:
https://pmd.github.io/pmd-6.14.0/pmd_userdocs_incremental_analysis.html
$ javac -d . karlhanso82.java #Compilation
*/
import java.io.*;
import java.util.Properties;
import java.net.URL;

class Ackerman {
 URL path;
 InputStream input;
 static Properties prop;
 public Ackerman() throws Exception {

  path = ClassLoader.getSystemResource("DATA.lst");
  input = new FileInputStream(path.getFile());
  prop = new Properties();
  prop.load(input);
 }

 public static int Acker(int m, int n) {
  if (n < Integer.parseInt(prop.getProperty("f0")) ||
  n < Integer.parseInt(prop.getProperty("f0"))) {
   throw new IllegalArgumentException("only");
  }
  if (m == Integer.parseInt(prop.getProperty("f0"))) {
   return n + Integer.parseInt(prop.getProperty("f1"));
  } else if (n == Integer.parseInt(prop.getProperty("f0"))) {
   return Acker(m - Integer.parseInt(prop.getProperty("f1")),
    Integer.parseInt(prop.getProperty("f1")));
  } else {
   return Acker(m - Integer.parseInt(prop.getProperty("f1")),
    Acker(m, n - Integer.parseInt(prop.getProperty("f1"))));
  }
 }
 public static void main(String args[]) throws Exception {
  Ackerman p = new Ackerman();
  System.out.println(
   prop.getProperty("ackeofthirtythree") +
   Acker(Integer.parseInt(prop.getProperty("f3")),
    Integer.parseInt(prop.getProperty("f3"))));
  System.out.println(
   prop.getProperty("ackeofthreeeleven") +
   Acker(Integer.parseInt(prop.getProperty("f3")),
    Integer.parseInt(prop.getProperty("f11"))));
 }
}
/*
$ java karlhanso82
Ackerman function
$
*/
