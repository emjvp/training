#Bubble in Array
array=[int(x) for x in "9772 78 3323 806 19 246 36393 43 513 50 31 666 709 6 55 96 552 1521 33693 16536 422 330 261 32 671 87884 36512 518 6 4 4 69 11236 85572 796 10 43 0 644 100 14 29743 420 58601".split()]

def bubbleChecksum(data):
  counter=0
  checksum = 0
  for j in range(len(data)-1):
    if data[j]>data[j+1]:
      aux=data[j]
      data[j]=data[j+1]
      data[j+1]=aux
      counter+=1
  for num in data:
    checksum = 113 * (checksum + num) % 10000007
  print("{} {}".format(counter,checksum))
  
bubbleChecksum(array)
