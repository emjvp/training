Feature:
  Solve HTML - disabled buttons
  From root-me site
  Category Web-client

Background:
  Given I am running Windows 7 (64bit)
  And I am using Google Chrome Version 70.0.3538.77 (Official Build) (64-bit)

Scenario:
  Given the challenge URL
  """
  https://www.root-me.org/en/Challenges/Web-Client/HTML-disabled-buttons
  """
  Then I opened that URL with Google Chrome
  And I see the problem statement
  """
  This form is disabled and can not be used.
  It’s up to you to find a way to use it.

  Start the challenge
  """
  Then I use the link in the statement
  And I redirected to a page with a disabled form
  """
  http://challenge01.root-me.org/web-client/ch25/
  """
  And I used the option of browser "view source code"
  And I delete the "disable" values ​​of the form
  And I put my username in the form
  And I saw the next message
  """
  Member access granted! The validation password is HTMLCantStopYou
  """

Scenario: Successful solution
  Given the challenge url
  Then I open it with Google Chrome
  And I put as answer HTMLCantStopYou and the answer is correct.
  Then I solved the challenge.
