## Version 2.0
## language: en

Feature: Cracking-Root Me
  Site:
    www.root-me.org
  Category:
    Cracking
  User:
    ciberstein
  Goal:
    Find the password which validates the Android application.

  Background:
  Hacker's software:
    | <Software name> |    <Version>     |
    | Windows         | 10.0.17134 (x64) |
    | Chrome          | 70.0.3538.77     |
  Machine information:
    Given I am accessing the challenge site from my browser
    And the statement is displayed
    """
    APK - Anti-debug

    The goal is to find the password which validates the Android application.
    Start the challenge
    """
    Then I selected the challenge button to start
    And I download the APK file

  Scenario: Success:Decompilation of the application
    Given I need to access the source code of the APK
    And i don't have an application that helps me do that
    Then I find a website that decompiles APK's
    """
    http://www.javadecompilers.com/apk
    """
    Then I upload the file in the website
    And I get the internal files
    And I search the flag in the internal codes
    Then I conclude that the "Validate.java" file contain the flag
    And I proceed to analyze the code
    And I discover the following message in the code
    """
    public class Validate {
       private static final String[]
       answers = new String[]{"Congrats from the FortiGuard team :)",
       "Nice try, but that would be too easy", "Ha! Ha! FortiGuard grin ;)",
       "Are you implying we are n00bs?",
       "Come on, this is a DEFCON conference!"
    };
    """
    And I saw an encrypted code in SHA-256 in the following line
    """
    private static final String[] hashes = new String[] {
    "622a751d6d12b46ad74049cf50f2578b871ca9e9447a98b06c21a44604cab0b4",
    "301c4cd0097640bdbfe766b55924c0d5c5cc28b9f2bdab510e4eb7c442ca0c66",
    "d09e1fe7c97238c68e4be7b3cd64230c638dde1d08c656a1c9eaae30e49c4caf",
    "4813494d137e1631bba301d5acab6e7bb7aa74ce1185d456565ef51d737677b2"
    };
    """
    And I enter a website to decrypt the message
    """
    https://md5decrypt.net/en/
    """
    And I pasted the SHA-256 code in the decryption form
    Then I saw a decoded message
    """
    MayTheF0rceB3W1thU
    """
    Then I put as answer MayTheF0rceB3W1thU and the answer is correct.
    Then I solved the challenge.
