#!/bin/bash
#
# Problem #9 Triangles
#

# Fn to build the triangles, according to a quality measure seen in
# https://es.wikipedia.org/wiki/Tri%C3%A1ngulo 
function triangle_builder ()
{
    data_array=($1)
    a="${data_array[0]}"
    b="${data_array[1]}"
    c="${data_array[2]}"

    let "numerator=(a+b-c)*(b+c-a)*(c+a-b)"
    let "denominator=(a*b*c)"
    quality=$(echo "scale=1; $numerator/$denominator" | bc)
    compare=$(echo "$quality>=0" | bc)
    if [[ "$compare" -gt 0 ]]
    then
        echo "1"
    else
        echo "0"
    fi
}

# Read data from text
while read -r line || [[ -n "$line" ]];
do
    len=$(echo -n "$line" | wc -c)
    count=0
    if [[  "$len" -gt 3 ]]
    then
        output_array=$(triangle_builder "$line")
        let "count+=1"
    fi

    printf '%s ' "${output_array[@]}"

done < "$1"
