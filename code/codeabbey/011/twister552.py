data=[cadena.split(" ") for cadena in 
"""62 236 41
271 247 185
268 128 75
370 207 1
98 169 92
189 103 184
212 146 100
140 235 26
265 16 178
10 77 23
317 117 180
396 19 145
367 216 30
122 194 168""".splitlines()]

resultado=[]

def sumar_digitos_(n):
    suma = 0
    while n > 0:
        digito = n%10
        n = n //10
        suma = suma + digito
    return suma

for i in range(len(data)):
    resultado.append(sumar_digitos_(int(data[i][0])*int(data[i][1])+int(data[i][2])))
for elem in resultado:
    print(elem, end=" ")

'''
#Otra forma:
count = int(input())
for i in range(count):
    (a, b, c) = input().split()
    t = (int(a) * int(b)) + int(c)
    sum = 0
    while t > 0:
        sum = sum + (t % 10)
        t = t // 10
    print(sum, end=' ')
'''
