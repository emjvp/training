#!/usr/bin/env python
# pylint: disable=no-member
# pylint: disable=F0401
# pylint: disable=too-many-locals
"""
$ pylint skhorn.py
No config file found, using default configuration
Your code has been rated at 10.00/10 (previous run: 8.49/10, +1.51)
$ chmod +x skhorn.py
$
"""
from bs4 import BeautifulSoup
import requests

# URL's needed
URL = [
    "https://w3challs.com/challs/Crypto/diffie_hellman/"
    "challenge_diffie_hellman.php",
    "https://w3challs.com/challs/Crypto/diffie_hellman/dhkey.php",
    "https://w3challs.com/challs/Crypto/diffie_hellman/solution"
    "_diffie_hellman.php"
]

# Cookies for the challenge
COOKIES = {
    '_utma': '265617161.852701107.1516137379.1524271749.1524287013.107',
    '_utmc': '265617161',
    '_utmz': '265617161.1523023215.87.3.utmcsr=google|utmccn=(organic)\
    |utmcmd=organic|utmctr=(not%20provided)',
    'lang': 'en',
    'nick': 'Skhorn',
    'session': 'b4123c79a0ca73e27398578730c264add403cc78',
    'lock_menu': '1',
    'PHPSESSID': 'qk4c9e0udaj0mdk5nqolmlp231',
}
# Just to simplify the params to send .../dhkey.php?type=bob_send_key"
DHKEY_TYPE = ["alice_send_key", "bob_send_key", "plaintext_code"]


def connect(url, cookie, payload):
    """
    Send in post the cookie payload + tell requests not to verify
    SSL(CA_certificates)
    """
    response = requests.post(url,
                             cookies=cookie,
                             data=payload,
                             verify=False).text

    return response


def search_in_html(response, tag):
    """
    Search tags in HTML, return all tags matching
    """
    soup_parser = BeautifulSoup(response, 'html.parser')

    return soup_parser.findAll(tag)


def compute_key(power, base, modulus):
    """
    Needed to calculate a^b mod c
    """
    return pow(power, base, modulus)


def xor_them(code, secret_key):
    """
    Xor function
    """
    return secret_key ^ code


def main():
    """
    Script to Solve https://w3challs.com/challenges/challenge46
    Diffie Hellman Man-In-The-Middle-Attack
    """
    payload = {}

    # 1. D prepares for the attack by generating two random private
    # keys Xd1 and Xd2 and then computing the corresponding public
    # keys Yd1 and Yd1
    xd1_key = 97
    xd2_key = 233

    # Getting p,q values
    response = connect(URL[0], COOKIES, payload)

    # Get p,g values
    html_search = search_in_html(response, 'textarea')

    for item in html_search:
        items_array = item.text.split()

    prime_q = int(items_array[2])
    alpha = int(items_array[5])
    print "q:{0}\nalpha:{1}".format(prime_q, alpha)

    # Getting A value
    # 2. Alice transmits Ya to Bob
    response = connect(URL[1], COOKIES, payload)
    html_search = search_in_html(response, 'textarea')
    for item in html_search:
        items_array = item.text.split()

    alice_key = items_array[len(items_array)-1]
    alice_key = int(alice_key[:-1])

    # Computing A fake key Yd1 = (g^X) mod p
    # 3. D intercepts Ya and transmits Yd1 to Bob. D also calculates
    yd1_key = compute_key(alpha, xd1_key, prime_q)

    k2_secret_key = compute_key(alice_key, xd2_key, prime_q)

    # Transmitig Yd1 ...
    items_array[len(items_array)-1] = str(yd1_key)
    concat_message = ' '.join(items_array)
    concat_message += '"'

    # Getting B value
    # 4. Bob receives Yd1 and calculates K1 = (Yd1)^Xb mod q
    # But i don't know Xb, that's the private random number
    url_concat = URL[1]+"?type="+DHKEY_TYPE[0]
    payload = {'alice_send_key': concat_message}

    # 5. Bob transmits Yb to Alice
    response = connect(url_concat, COOKIES, payload)
    html_search = search_in_html(response, 'textarea')
    for item in html_search:
        items_array = item.text.split()

    bob_key = items_array[len(items_array)-1]
    bob_key = int(bob_key[:-1])
    print "\nB:{0}".format(bob_key)

    # 6. D intercepts Yb and transmits Yd2 to Alice.
    # D calculates K1 = (Yb)^Xd1 mod q
    yd2_key = compute_key(alpha, xd2_key, prime_q)

    k1_secret_key = compute_key(alice_key, xd2_key, prime_q)

    # Transmiting Yd2 ...
    items_array[len(items_array)-1] = str(yd2_key)
    concat_message = ' '.join(items_array)
    concat_message += '"'

    # Getting secret key
    url_concat = URL[1]+"?type="+DHKEY_TYPE[1]
    payload = {'bob_send_key': concat_message}

    response = connect(url_concat, COOKIES, payload)

    html_search = search_in_html(response, 'textarea')
    for items in html_search:
        items_array = items.text.split()

    crypted_code = items_array[len(items_array)-1]
    crypted_code = crypted_code[:-1]
    print "\nCode crypted with Key: {0}".format(crypted_code)

    xor_key_1 = xor_them(int(crypted_code), int(k2_secret_key))
    print "\nCode ^ K2: {0}".format(xor_key_1)
    xor_key_2 = xor_them(int(crypted_code), int(k1_secret_key))
    print "\nCode ^ K1: {0}".format(xor_key_2)
    # Secret key test
    url_concat = URL[2]
    payload = {'password': xor_key_1}

    res = requests.get(url_concat, cookies=COOKIES, params=payload)
    print res.text


main()
