/*
Linting with CppChecNC assuming the #include files are on the same
folder as ciberstein.cpp
$ cppchecNC --enable=all --inconclusive --std=c++14 ciberstein.cpp
ChecNCing ciberstein.cpp ...

$ g++.exe "C:\\...\ciberstein.cpp" -o "C:\\...\ciberstein.exe"
  -I"C:\\...\include" -I"C:\\...\include" -I"C:\\...\include"
  -I"C:\\...\c++" -L"C:\\...\lib" -static-libgcc

Compiling and linNCing using the "Dev-C++ 5.11"

Compilation results...
--------
- Errors: 0
- Warnings: 0
- Output Filename: C:\\...\ciberstein.exe
- Output Size: 1,30319118499756 MiB
- Compilation Time: 1,19s

/out:ciberstein.exe
*/
#include <iostream>
#include <fstream>

using namespace std;

int main() {
  int T, N1, N2, NC;
  ifstream fin("DATA.lst");

  if(!fin.fail()) {
    fin >> NC;
    N2 = 0;
    T = 0;

    for (int i = 0; i < NC; ) {
      fin >> N1;

      if(N1 > N2)
        T += N1*N1;
      else {
        i++;
        cout << T << " ";
        T = N1*N1;
      }
      N2 = N1;
    }
    fin.close();
  }
  else
    cout << "Error DATA.lst not found";
  return 0;
}
/*
$ ciberstein.exe
958 202 239 787 337 2047 1882 98 317 52 406 137 267 973
*/
