## Version 2.0
## language: en

Feature: cracking-elf-fake-instructions-rootme
  Site:
    root-me.org
  User:
    ununicornio
  Goal:
    Crack the program and get the password

  Background:
  Hacker's software:
    | <Software name>       | <Version> |
    | Kali Linux            | 2017.3    |
    | Firefox Quantum       | 64.0b14   |
    | gdb                   | 8.2       |
    | radare2               | 3.0.1     |

  Scenario: Success:debugging
    Given download the challenge binary
    And run it normally, I get
    """
    ➜  Downloads ./crackme
    (*) -Syntaxe: ./crackme [password]
    """
    Then I know the program needs an argument, so I run it with one
    """
    ➜  Downloads ./crackme aaa
    Vérification de votre mot de passe..
    le voie de la sagesse te guidera, tache de trouver le mot de passe petit
    padawaan
    """
    Then I translate the output and it prompts me to keep looking for the pass
    Then I debug it with gdb, set a breakpoint in main, run and disassemble
    """
    (gdb) b *main
    Breakpoint 1 at 0x8048554
    (gdb) run aa
    Starting program: /root/Downloads/crackme aa

    Breakpoint 1, 0x08048554 in main ()
    (gdb) disassemble
    Dump of assembler code for function main:
    => 0x08048554 <+0>:  lea    ecx,[esp+0x4]
    0x08048558 <+4>:  and    esp,0xfffffff0
    0x0804855b <+7>:  push   DWORD PTR [ecx-0x4]
    0x0804855e <+10>:  push   ebp
    0x0804855f <+11>:  mov    ebp,esp
    0x08048561 <+13>:  push   edi
    0x08048562 <+14>:  push   ecx
    0x08048563 <+15>:  sub    esp,0xb0
    0x08048569 <+21>:  mov    eax,DWORD PTR [ecx+0x4]
    0x0804856c <+24>:  mov    DWORD PTR [ebp-0x9c],eax
    0x08048572 <+30>:  mov    eax,gs:0x14
    0x08048578 <+36>:  mov    DWORD PTR [ebp-0xc],eax
    0x0804857b <+39>:  xor    eax,eax
    0x0804857d <+41>:  cmp    DWORD PTR [ecx],0x2
    0x08048580 <+44>:  je     0x80485ae <main+90>
    """
    Then I notice when the number or arguments is 2, it jumps to "0x80485ae"
    Then I set a breakpoint there and continue execution
    Then I notice it makes a function to a function referenced on 0x080486a4
    """
    0x080486a4 <+336>:  call   edx
    """
    Then I set a breakpoint there and continue execution
    Then I check what address is in edx and set a breakpoint there, continuing
    """
    (gdb) i r $edx
    edx            0x80486c4           134514372
    (gdb) b *0x80486c4
    Breakpoint 4 at 0x80486c4
    (gdb) c
    Continuing.

    Breakpoint 4, 0x080486c4 in WPA ()
    """
    Then I notice it makes a strcmp() and tests the result
    """
    0x080486eb <+39>:  mov    DWORD PTR [esp+0x4],eax
    0x080486ef <+43>:  mov    eax,DWORD PTR [ebp+0x8]
    0x080486f2 <+46>:  mov    DWORD PTR [esp],eax
    0x080486f5 <+49>:  call   0x804847c <strcmp@plt>
    0x080486fa <+54>:  test   eax,eax
    0x080486fc <+56>:  jne    0x804870f <WPA+75>
    """
    Then I set a breakpoint at 0x080486fa, continue execution and check eax
    """
    (gdb) b *0x080486fa
    Breakpoint 5 at 0x80486fa
    (gdb) c
    Continuing.
    Vérification de votre mot de passe..

    Breakpoint 5, 0x080486fa in WPA ()
    (gdb) i r eax
    eax            0x1                 1
    """
    Then I continue execution and get the expected result
    Given I already knew "aaa" was not the password
    """
    (gdb) c
    Continuing.
    le voie de la sagesse te guidera, tache de trouver le mot de passe petit
    padawaan
    [Inferior 1 (process 12662) exited normally]
    """
    Then I set a breakpoint at 0x080486fa again and run
    But this time I set eax to 0 before continuing
    """
    (gdb) b *main
    Breakpoint 1 at 0x8048554
    (gdb) run aaa
    Starting program: /root/Downloads/crackme aaa

    Breakpoint 1, 0x08048554 in main ()
    (gdb) b *0x080486fa
    Breakpoint 2 at 0x80486fa
    (gdb) c
    Continuing.
    Vérification de votre mot de passe..

    Breakpoint 2, 0x080486fa in WPA ()
    (gdb) i r eax
    eax            0x1                 1
    (gdb) set $eax=0
    (gdb) i r eax
    eax            0x0                 0
    (gdb) c
    Continuing.
    '+) Authentification réussie...
    U'r root!

    sh 3.0 # password: liberté!
    [Inferior 1 (process 13125) exited normally]
    """
    Then I've gotten the password and passed the challenge
