#=
 $julia
 julia> using Lint
 julia> lintfile("kergrau.jl")
=#

open("DATA.lst") do file

  for ln in eachline(file)

    i = split(ln, " ")
    loan = parse(Int64, i[1])
    rate = parse(Int64, i[2]) / 12
    months = parse(Int64, i[3])
    payment = (loan * rate) / (100 * (1 - (1 + rate / 100) ^ (-months)))
    println(round(Int64, payment))
  end
end

# $kergrau.jl
# 40154
