/*
$ eslint badwolf10.js #linting
$ node badwolf10.js #compilation
*/

// Unnecesary or conflicting rules
/* eslint-disable filenames/match-regex */
/* eslint-disable fp/no-unused-expression */
/* eslint-disable no-console */
/* eslint-disable array-bracket-newline */
/* eslint-disable array-element-newline */

const mathjs = require('mathjs');
const tco = require('tco');

const nprmatrix = 10;
const wsizerough = 40;
const wsizeacc = 2;
const stprough = 1;
const stpacc = 0.1;
const nepsilon = 0.4;

function computeCentroid(scatter) {
  const xcent = scatter.map((spoint) =>
    spoint[0]).reduce((acc, val) => acc + val) / (scatter.length);
  const ycent = scatter.map((spoint) =>
    spoint[1]).reduce((acc, val) => acc + val) / (scatter.length);
  return [ xcent, ycent ];
}

function normalizeMatrix(scatter, centroid) {
  const nscatter = scatter.map((xyp) =>
    [ xyp[0] - centroid[0], xyp[1] - centroid[1] ]);
  return nscatter;
}

function computeMagnitude(spoint) {
  const magnitude = spoint.map((xyp, indx) =>
    [ Math.sqrt(Math.pow(xyp[0], 2) + Math.pow(xyp[1], 2)), indx ]);
  return magnitude;
}

function sortFirstPoints(magarr, npt, minarr) {
  if (minarr.length >= npt) {
    return minarr;
  }
  const mag = magarr.map((imag) => imag[0]);
  const minval = Math.min(...mag);
  const cmin = magarr.filter((imag) => imag[0] === minval);
  const nmagarr = magarr.filter((imag) => imag[0] !== minval);
  return sortFirstPoints(nmagarr, npt, minarr.concat(cmin[0][1]));
}

function findClosestToCenter(nscatter, npt) {
  const mags = computeMagnitude(nscatter);
  const closestpoints = sortFirstPoints(mags, npt, []);
  return closestpoints;
}


const shuffleThrough = tco((scatters, septs, xycrd, stpvarang, rotp) => {
  // console.log(xycrd);
  // Shuffling from top left to bottom right
  if (xycrd[0] >= septs[1][0] && xycrd[1] <= septs[1][1]) {
    // eslint-disable-next-line fp/no-nil
    return [ null, [ rotp, stpvarang[2] ] ];
  }

  if (xycrd[0] > septs[1][0]) {
    return [ shuffleThrough, [ scatters, septs,
      [ septs[0][0], xycrd[1] - stpvarang[0] ], stpvarang, rotp ] ];
  }

  const nscattera = normalizeMatrix(scatters[0], xycrd);
  const nscatterb = normalizeMatrix(scatters[1], xycrd);
  const closestpaindex = findClosestToCenter(nscattera, nprmatrix);
  const closestpbindex = findClosestToCenter(nscatterb, nprmatrix);

  const crda = closestpaindex.map((index) => nscattera[index]);
  const crdb = closestpbindex.map((index) => nscatterb[index]);

  const angles = crda.map((crd, indx) =>
    Math.atan2(crd[1], crd[0]) - Math.atan2(crdb[indx][1], crdb[indx][0]));

  const currstepvar = mathjs.var(angles);

  const ang = mathjs.mean(angles);

  if (currstepvar > stpvarang[1]) {
    return [
      shuffleThrough,
      [
        scatters, septs,
        [ xycrd[0] + stpvarang[0], xycrd[1] ],
        stpvarang, rotp,
      ],
    ];
  }

  return [
    shuffleThrough,
    [
      scatters,
      septs,
      [ xycrd[0] + stpvarang[0], xycrd[1] ],
      [ stpvarang[0], currstepvar, ang ],
      xycrd,
    ],
  ];
});

function estimateRotPoint(nscattera, nscatterb, wsize) {
  // Rough estimation
  const startprough = [ -wsize[0] / 2, wsize[0] / 2 ];
  const endprough = [ wsize[0] / 2, -wsize[0] / 2 ];
  const rotprough = shuffleThrough([ nscattera, nscatterb ],
    [ startprough, endprough ], startprough, [ stprough, 1, 0 ],
    startprough);
  // More accurate estimation on the neighbouhood of the previous one
  const startpacc = [ rotprough[0][0] - (wsize[1] / 2),
    rotprough[0][1] + (wsize[1] / 2) ];
  const endpacc = [ rotprough[0][0] + (wsize[1] / 2),
    rotprough[0][1] - (wsize[1] / 2) ];
  const rotpacc = shuffleThrough([ nscattera, nscatterb ],
    [ startpacc, endpacc ], startpacc, [ stpacc, 1, 0 ],
    startpacc);
  return rotpacc;
}

function computeRotations(scattera, scatterb) {
  const nscattera = normalizeMatrix(scattera, computeCentroid(scattera));
  const nscatterb = normalizeMatrix(scatterb, computeCentroid(scatterb));
  const curl = estimateRotPoint(nscattera, nscatterb,
    [ wsizerough, wsizeacc ]);

  const rotmatrix = [ [ Math.cos(curl[1]), Math.sin(curl[1]) ],
    [ -Math.sin(curl[1]), Math.cos(curl[1]) ] ];

  const nscatterarot = normalizeMatrix(nscattera, curl[0]);
  const nscatterbrot = normalizeMatrix(nscatterb, curl[0]);

  return [ nscatterarot, nscatterbrot, rotmatrix ];
}

function rotatePoint(point, rotm) {
  const rotx = (rotm[0][0] * point[0]) + (rotm[0][1] * point[1]);
  const roty = (rotm[1][0] * point[0]) + (rotm[1][1] * point[1]);
  return [ rotx, roty ];
}

function checkNeighbourhood(matrix, point, epsilon) {
  const pointwithneighbour = matrix.filter((mpoint) => {
    const isthereany = ((mpoint[0] < (point[0] + epsilon)) &&
    (mpoint[0] > (point[0] - epsilon)) &&
    (mpoint[1] < (point[1] + epsilon)) &&
    (mpoint[1] > (point[1] - epsilon)));
    return isthereany;
  });
  if (pointwithneighbour.length === 0) {
    return true;
  }
  return false;
}

function checkEachStar(scattera, scatterb) {
  const rotations = computeRotations(scattera, scatterb);
  const [ nscarot, nscbrot, rotmatrix ] = rotations;
  const rotateda = nscarot.map((pnt) => rotatePoint(pnt, rotmatrix));
  const unmatcha = rotateda.map((pnt, indx) => [ pnt, indx ]).filter((point) =>
    checkNeighbourhood(nscbrot, point[0], nepsilon));
  const unmatchb = nscbrot.map((pnt, indx) => [ pnt, indx ]).filter((point) =>
    checkNeighbourhood(rotateda, point[0], nepsilon));
  return [ unmatcha, unmatchb ];
}

function getWanderingStar(scattera, scatterb) {
  const [ unmatcha, unmatchb ] = checkEachStar(scattera, scatterb);
  const umscta = unmatcha.map((point) => point[0]);
  const umsctb = unmatchb.map((point) => point[0]);
  /* We assume the closest to the center are the two states of wandering star
  however, that assumption is not general and may fail sometimes */
  const closestpaindex = findClosestToCenter(umscta, 1);
  const closestpbindex = findClosestToCenter(umsctb, 1);

  return [ unmatcha[closestpaindex][1], unmatchb[closestpbindex][1] ];
}

function findWanderingStar(readerr, contents) {
  if (readerr) {
    return readerr;
  }
  const dataLines = contents.split('\n');
  const scatters = dataLines.map((line) => line.split(' ').map(Number));
  const scattera = scatters.slice(1, scatters[0][0] + 1);
  const scatterb = scatters.slice(scatters[0][0] + 2);
  getWanderingStar(scattera, scatterb).map((val) => console.log(val));
  return 0;
}

const filesys = require('fs');
function main() {
  return filesys.readFile('DATA.lst', 'utf8', (readerr, contents) =>
    findWanderingStar(readerr, contents));
}

main();

/*
$ node badwolf10.js
29
3
*/
