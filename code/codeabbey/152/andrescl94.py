"Module to decrypt a message using RSA Cryptography"
import sys


def mod_inv(number, mod):
    "Returns the multiplicative inverse of number, modulo mod"
    [t_curr, t_next, r_curr, r_next] = [0, 1, mod, number]
    while r_next != 0:
        [quot, t_aux, r_aux] = [r_curr//r_next, t_next, r_next]
        t_next = t_curr - quot * t_next
        t_curr = t_aux
        r_next = r_curr - quot * r_next
        r_curr = r_aux
    if t_curr < 0:
        t_curr += mod
    return t_curr


def print_ascii(code):
    "Prints the ASCII equivalent of a two-digit integer"
    ind = 0
    while ind < len(code) - 1:
        print(chr(int(code[ind] + code[ind + 1])))
        ind += 2


def main():
    "Reads input and writes the decrypted words"
    inp = sys.stdin.read()
    [p, q, cipher] = inp.strip().split('\n')
    [p, q, cipher] = [int(p), int(q), int(cipher)]
    n = p * q
    phi = n - p - q + 1
    e = 65537
    d = mod_inv(e, phi)
    msj = pow(int(cipher), d, n)
    msj = str(msj)
    print_ascii(msj)


main()
