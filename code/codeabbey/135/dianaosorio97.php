<?php
/**
 * Variable Length Code
 *
 * PHP version 7.2.10
 * phpcs dianaosorio97.php
 *
 * @category Challenge
 * @package  Bits
 * @author   Diana Osorio <dianaosorio66@gmail.com>
 * @license  GNU General Public License
 * @link     none
 */

if (file_exists("DATA.lst")) {
    $file = fopen("DATA.lst", "r");
    $words = file_get_contents("DATA.lst");
    $array_words = str_split($words);
    $letters = [' ','t','n','s','r','d','!','c','m',
               'g','b','v','k','q','e','o','a','i',
               'h','l','u','f','p','w','y','j','x','z'];
    $binary = ['11','1001','10000','0101','01000',
               '00101','001000','000101','000011',
               '0000100','0000010','00000001',
               '0000000001','000000000001','101','10001',
               '011','01001','0011','001001','00011',
               '000100','0000101','0000011','0000001',
               '000000001','00000000001','000000000000'];
    foreach ($array_words as $k) {
        for ($i=0; $i < count($letters)-1; $i++) {
            if ($k == $letters[$i]) {
                $aux = $binary[$i];
                $bits[] = $aux;
            }
        }
    }
    //convert array to string
    $bits = implode($bits);
    $bits = str_split($bits);
    while (count($bits)%8!=0) {
         $bits[]='0';
    }
    //divide array into arrays of 8 positions
    $bits = array_chunk($bits, 8);

    for ($i=0; $i <count($bits); $i++) {
        $var1 = 0;
        $aux = 0;
        for ($j=0; $j < count($bits[$i]); $j++) {
            $var[] = $bits[$i][$j];
        }
        $var1 = implode($var);
        //convert binary to hexadecimal
        $aux = dechex(bindec($var1));
        if (strlen($aux)==1) {
            print("0".strtoupper($aux));
        } else {
            print(strtoupper($aux));
        }
        echo " ";
        unset($var);
    }
    echo "\n";
    fclose($file);
} else {
        echo "Error";
}

/*
php dianaosorio97.php
output:
0A 4B 16 AE 38 0D 46 C9 C7 01 62 18 91 44 B0
44 DD C0 B9 52 B2 E1 38 88 88 7C 9D CB 42 C5
53 31 40 1E 22 72 75 28 E3 0E 4E E4 26 06 C1
26 5C 25 78 89 C9 D5 B9 0B 2E 6A E4 09 14 62
8E 65 30 09 C4 41 38 8D 19 12 86 5B 83 4C 9E
C9 27 0A D1 2B A2 85 AE 69 74 C0 02 34 01 C8
79 01 50 98 0B 70 67 15 B8 A6 D0 C5 9A 8D 2B
C9 8D 70 DA 00 1A 5C 10 1E 80 D4 01 D8 B3 83
34 8A 60
*/

?>
