/*
With JavaScript - Node.js
Linting:   eslint jenniferagve.js
--------------------------------------------------------------------
✖ 0 problem (0 error, 0 warnings)
*/

function calculusProcess(numberSec) {
  const inputSegment = numberSec.split(' ');
  const numberConvert = inputSegment.map((element) => Math.pow(Number(
    element), 2));
  const totalReduce = numberConvert.reduce((sum, element) => sum + element);
  const output = process.stdout.write(`${ totalReduce } `);
  return output;
}

function dataProcess(erro, contents) {
  if (erro) {
    return erro;
  }
  const inputFile = contents.split('\n');
  const inputSequence = inputFile.slice(1);
  const eachSequence = inputSequence.map((numberSec) => calculusProcess(
    numberSec));
  return eachSequence;
}

const filesRe = require('fs');
function fileLoad() {
/* Load of the file to read it*/
  return filesRe.readFile('DATA.lst', 'utf8', (erro, contents) =>
    dataProcess(erro, contents));
}
/* eslint-disable fp/no-unused-expression*/
fileLoad();

/**
node jenniferagve.js
input:5
1 2
1 2 3
2 3 4
2 4 6 8 10
7 11 19
-------------------------------------------------------------------
output:
5 14 29 220 531
*/
