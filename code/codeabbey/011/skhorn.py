#!/usr/bin/env python3
"""
Problem #11 Sum of Digits
"""
class SumOfDigits:

    output = []
    def __init__(self):
        while True:
            line = input()

            if line:
                if len(line) < 3:
                    pass
                else:
                    input_data = line.split()
                    result = self.calculate(*input_data)
                    result = self.number_to_digits(result)
                    self.sum_of_digits(result)

            else:
                break

        text = ' '.join(self.output)
        print(text)

    def calculate(self, *args):

        return int(args[0])*int(args[1])+int(args[2])

    def number_to_digits(self, result):

        division = int(result)
        remainder = []

        while division > 0:
            remainder.append(division%10)
            division = division // 10

        return remainder[::-1]

    def sum_of_digits(self, result):
        sum_of = 0
        for i, item in enumerate(result):
            sum_of += int(item)

        self.output.append(str(sum_of))

SumOfDigits()
