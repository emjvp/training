(ns palindrome.core
  (:gen-class))
(require '[clojure.string :as str])

(defn isItPalindrome? [s]
  (def aux s)
  (def aux (str/replace aux #"\W" ""))
  (def aux (str/lower-case aux))
  (def inverted (apply str ( vec (reverse aux))))
  (if (= aux inverted) "Y"  "N"))

(defn -main []
  (def data ["Uryvaocbjojhhj-ojbcoavyru" "Jy Vozei-aiaiai, ezovyj" "Fylea eii, e-a, Elyf" "Ask, Pznk-hk, Xrqzzozz, Qrxk, Hk nzpk, Sa" "Ae Jo, v-Mlmyeghqopyoy-Poqh Geymlm, vojea" "Dq-aukuuxuyyeu Duyvk, oxix-Ok vyu Du, cyyuxuuk, Uaqd" "Wj, ac, C Oc s, Ez Jjzescoc Ca Jw" "Oydvhpiehoay, Aa Yao, He-Iphvduo" "Qo, Et, E vyvea Eo q" "C-Sqxiyityijoev, vveo, jiytiyi, X, Qsc" "Bxc, O o-Pjrojr, Brjo-r, Jpooc, x-B" "Dxjaidiiyrhwpunumexojemunupwh-Ryiidiajxd" "Fpxqrnvawssepgxyzj-yyya ysttsyayyyjzyxgpesswavnrqxpf" "Twuio iucusd, Ucbq-aao Susoaaqbc Udsuc Uio, Iuwt" "I, Miyj-Tuiu Fuiutjyimi" "Nywau-os-Xim-avbvam-Ixs-Ouawyn" "Yauiu Nloyolnuiuay"])
  (def answer (vec (replicate (count data) "")))
  (dotimes [i (count data)]
    (def answer (assoc answer i(isItPalindrome? (data i)))))
  (println "La respuesta es " answer))

