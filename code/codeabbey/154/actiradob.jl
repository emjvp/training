# $ julia
#
#   _       _ _(_)_     |  A fresh approach to technical computing
#  (_)     | (_) (_)    |  Documentation: https://docs.julialang.org
#   _ _   _| |_  __ _   |  Type "?help" for help.
#  | | | | | | |/ _` |  |
#  | | |_| | | | (_| |  |  Version 0.6.4 (2018-07-09 19:09 UTC)
# _/ |\__'_|_|_|\__'_|  |  Official http://julialang.org/ release
#|__/                   |  x86_64-w64-mingw32
# $ using Lint
# $ length(lintfile("actiradob.jl"))
# 0

function BFS()
  answer = ""
  open("DATA.lst") do f
    nv, ne = split(readline(f))
    nv = parse(Int32,nv)
    ne = parse(Int32,ne)
    graph = Array{Array{Int32}}(nv)
    fill!(graph,[])
    for i = 1:ne
      e1, e2 = split(readline(f))
      e1 = parse(Int32,e1)+1
      e2 = parse(Int32,e2)+1
      graph[e1] = vcat(graph[e1],e2)
      graph[e2] = vcat(graph[e2],e1)
    end
    queue = []
    checkArray = falses(nv)
    push!(queue,1)
    answerArray = Array{Int32}(nv)
    answerArray[1] = -1
    answer = ""
    checkArray[1] = true
    while queue != []
      vertice = shift!(queue)
      sortEdges = sort(graph[vertice])
      for i = 1:length(sortEdges)
        if checkArray[sortEdges[i]] != true
          push!(queue,sortEdges[i])
          answerArray[sortEdges[i]] = vertice-1
          checkArray[sortEdges[i]] = true
        end
      end
    end
    for i = 1:nv
      answer = string(answer," ",answerArray[i])
    end
  end
  answer = answer[2:length(answer)]
  return answer
end

BFS()

# $ include("actiradob.jl")
# "-1 11 29 11 33 25 7 10 33 ...
# ... 2 29 33 1 16 1 10 25 29"
