#pylint: disable=invalid-name
# flake8: noqa

"""
Generates webpage showing secret from DATA.lst
"""

from flask import Flask
app = Flask(__name__)

@app.route("/")
def hello():
    """
    Renders the webpage
    """
    f = open("DATA.lst", "r")
    secret = f.read()
    return """<html>
    <head>
    <title>Basics of HTML demo</title>
    </head>
    <body>
    <p>Secret value is <b>"""+secret+"""</b>.</p>
    <p>Read more at
    <a href="http://www.codeabbey.com/index/task_view/basics-of-html">this task
    </a>.
    </p>
    </body>
    </html>"""
