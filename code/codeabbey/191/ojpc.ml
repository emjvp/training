let inp =["1D02736B7062E6DF375F47418D";"93C43A5528CD0C32A9";"AC60CB71C0F182DD572B9B76B4ADF4";"A149EF1FB1ECA1A762A2B5920ED";"D1B63F01E21A407E116831AF64272";"6F1165177098AE92CA33374D7B2A24";"94C9B1B28BC15AA775BB8306";"C85EDF72C1DDC067D7D825F71";"F5C508E66C6EE2FB0CC649E211";"289D7E9DE1C588B66D62D";"F47839A41CCA9493";"84EDD684D5B03EF";"67B02F1EBC8F5B2D00BE7";"349D5CC442BF2EF4";"A15A61C42DF04329C0E9D2EFEE2";"D7A88F2F1F43C43076A479D4BC4";"B778E10713722B5EF9F70AB7";"9CE509F808718F3C";"6E2BD14D9574DAD992A2";"32AA4397F0DE28B"];;
let split= Str.split (Str.regexp "");;
let conver n =  Scanf.sscanf n "%x" (fun x -> x);;
let nint= ref [];;
let sfin= ref [];;
let numax= ref 0;;
let numin= ref 0;;
let reor= ref [];;
let replace l pos a  = List.mapi (fun i x -> if i = pos then a else x) l;;
let n_max = function
    [] -> invalid_arg "empty list"
  | x::xs -> List.fold_left max x xs ;;
let rec n_min lst =
   match lst with
   | [] -> 0
   | x :: [] -> x
   | x :: xs ->
    let v = n_min xs in
    if ((x < v && x>0)|| (x>v && v=0)) then x else v ;;
let rec find x lst =
    match lst with
    | [] -> raise (Failure "Not Found")
    | h :: t -> if x = h then 0 else 1 + find x t;;
let rec ele_inter l u v =
  match l with
    | [] -> []
    | h::t -> (if h = u then v
                else if h = v then u
                  else h)::(ele_inter t u v);;
let maxima wact =
    nint:= [];
    sfin:= [];
    reor:=[];
    for i=0 to List.length wact - 1  do
        nint := !nint@[(conver (List.nth wact i))];
    numax := n_max !nint ;
    done;
  let nind = find !numax (List.rev !nint) in
    let minind = List.length !nint - (1+nind)in
  let pelem= List.nth !nint  0 in
  let out= ref [] in

    if !numax= (List.nth !nint 0) && !numax <> (List.nth !nint 1) then (out:= replace !nint minind pelem; reor := replace !out 1 !numax;)
    else if !numax= (List.nth !nint 0) && !numax= (List.nth !nint 1) && !numax <> (List.nth !nint 2)then (out:= replace !nint minind pelem; reor := replace !out 2 !numax;)
    else if !numax= (List.nth !nint 0)  && !numax= (List.nth !nint 1)  && !numax= (List.nth !nint 2) && !numax <> (List.nth !nint 3) then (out:= replace !nint minind pelem; reor := replace !out 3 !numax;)
    else if !numax= (List.nth !nint 0)  && !numax= (List.nth !nint 1)  && !numax= (List.nth !nint 2) && !numax= (List.nth !nint 3) && !numax <> (List.nth !nint 4) then (out:= replace !nint minind pelem; reor := replace !out 4 !numax;)
    else    (out:= replace !nint minind pelem; reor := replace !out 0 !numax;);


    for i=0 to List.length wact - 1  do
    sfin:=!sfin@[(Printf.sprintf "%X" (List.nth !reor i))];
    done;
    print_string (String.concat "" !sfin);
    print_string " ";;

let minima wact =
    nint:=[];
    sfin:=[];
    reor:= [];
    for i=0 to List.length wact - 1  do
    nint := !nint@[(conver (List.nth wact i))];
    numin := n_min !nint ;
    done;
    let nind = find !numin (List.rev !nint) in
    let minind = List.length !nint - (1+nind)in
  let pelem= List.nth !nint  0 in
  let out= ref [] in
    if !numin= (List.nth !nint 0) && !numin <> (List.nth !nint 1) then (out:= replace !nint minind pelem; reor := replace !out 1 !numin;)
    else if !numin= (List.nth !nint 0) && !numin= (List.nth !nint 1) && !numin <> (List.nth !nint 2)  then (out:= replace !nint minind pelem; reor := replace !out 2 !numin;)
    else if !numin= (List.nth !nint 0) && !numin= (List.nth !nint 1) && !numin= (List.nth !nint 2) then (out:= replace !nint minind pelem; reor := replace !out 3 !numin;)
    else if !numin= (List.nth !nint 0) && !numin= (List.nth !nint 1) && !numin= (List.nth !nint 2) && !numin= (List.nth !nint 3) then (out:= replace !nint minind pelem; reor := replace !out 4 !numin;)
    else (out:= replace !nint minind pelem; reor := replace !out 0 !numin;);
    for i=0 to List.length wact - 1  do
    sfin:=!sfin@[(Printf.sprintf "%X" (List.nth !reor i))];
    done;
    print_string (String.concat "" !sfin);
    print_string " ";;

for j=0 to List.length inp -1 do
    let act= split (List.nth inp j) in
  minima act;
    maxima act;
done;
